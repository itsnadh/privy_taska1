/// <reference types="cypress"/>

describe('Basic Test',()=> {
    it('The webpage loads, at least',()=>{
        cy.visit('https://codedamn.com')
    })

    it.only('Login page looks good',()=>{
        cy.viewport(1280,720)
        cy.visit('https://codedamn.com')
        cy.contains('Sign in').click()
        cy.contains('Sign in to codedamn').should('exist')
        cy.contains('Forgot your password?').should('exist')
        cy.contains('Remember me').should('exist')
    })
   
    it.only('The login page link work',()=>{
        cy.viewport(1280,720)
        cy.visit('https://codedamn.com')
        cy.contains('Sign in').click()
        cy.contains('Sign in to codedamn').should('exist')
        // cy.contains('Forgot your password?').should('exist')
        cy.contains('Forgot your password?').click()
        // cy.contains('Remember me').should('exist')
        console.log(111)
        cy.contains('Register an account').click
        cy.url().should('include','/password-reset')
        cy.go('back')
    })


    it.only('Login should display correct error',()=>{
        cy.viewport(1280,720)
        cy.visit('https://codedamn.com')
        cy.contains('Sign in').click()
        cy.get('[data-testid=username]').type('admin')
        cy.get('[data-testid=password]').type('admin')


    })





    // it('Every basic element exist on mobile',()=>{
    //     cy.viewport('iphone-x')
    //     cy.visit('https://codedamn.com')
    // })
    
        // cy.visit('https://codedamn.com')
             

        // cy.get('[data-testid=menutoggle').should('have.text','you')

        // cy.get('.asynccomponent > div > a')
        // cy.contains('Learn Programming').should('exist')

        //way 3
        // cy.get('data-testid=learnbtn')

        // cy.contains('div[id=root]').should('exist')
        // cy.get('div#root').should('exist')
        // cy.get('div#noroot').should('not.exist')

})